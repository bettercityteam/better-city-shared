using Reinforced.Typings.Attributes;

namespace BetterCity.SharedKernel.Model.Response
{
    [TsInterface]
    public class PlaceImage
    {
        public string PlaceId { get; set; }
        
        public string ImageId { get; set; }
        
        private byte[] Content { get; set; }
    }
}