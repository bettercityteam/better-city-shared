using Reinforced.Typings.Attributes;

namespace BetterCity.SharedKernel.Model.Response
{
    [TsInterface]
    public class PlaceGeometry
    {
        public string PlaceId { get; set; }
        public string Name { get; set; }
        public Point[] BoundingPolygon { get; set; }
        public Waypoint[] Waypoints { get; set; }
    }
}