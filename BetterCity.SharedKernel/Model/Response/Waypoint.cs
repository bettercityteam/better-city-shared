﻿using Reinforced.Typings.Attributes;

namespace BetterCity.SharedKernel.Model.Response
{
    [TsInterface]
    public class Waypoint : Point
    {
        public string Id { get; set; }
        public double RadiusMeters { get; set; }
        public string Name { get; set; }
    }
}