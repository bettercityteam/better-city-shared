using Reinforced.Typings.Attributes;

namespace BetterCity.SharedKernel.Model.Response
{
    [TsInterface]
    public class PlaceInfo
    {
        public string PlaceId { get; set; }
        public string Name { get; set; }
        public string Summary { get; set; }
        public double Rating { get; set; }
        public CategoryRating[] CategoryRatings { get; set; }
        public string[] ImageFiles { get; set; }
    }
}