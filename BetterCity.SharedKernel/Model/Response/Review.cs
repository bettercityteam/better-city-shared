﻿using Reinforced.Typings.Attributes;

namespace BetterCity.SharedKernel.Model.Response
{
    [TsInterface]
    public class Review
    {
        public string Id { get; set; }
        public string PlaceId { get; set; }
        public string[] LikedCategoriesNames { get; set; }
        public string[] DislikedCategoriesNames { get; set; }
        public string[] SkippedCategoriesNames { get; set; }
        public string Text { get; set; }
    }
}