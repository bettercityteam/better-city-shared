﻿using Reinforced.Typings.Attributes;

namespace BetterCity.SharedKernel.Model.Response
{
    [TsInterface]
    public class CategoryRating {
        public string Name { get; set; }
        public int PositiveReviews { get; set; }
        public int NegativeReviews { get; set; }
        public int Skipped { get; set; }
    }
}