﻿using Reinforced.Typings.Attributes;

namespace BetterCity.SharedKernel.Model.Response
{
    [TsInterface]
    public class ReviewCategory
    {
        public string Id { get; set; }
        public byte[] Image { get; set; }
        public string Name { get; set; }
    }
}