﻿using Reinforced.Typings.Attributes;

namespace BetterCity.SharedKernel.Model.Response
{
    [TsInterface]
    public class Point
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}