﻿using Reinforced.Typings.Attributes;

namespace BetterCity.SharedKernel.Model.Request
{
    [TsClass]
    public class NewReview
    {
        public string[] LikedCategories { get; set; }
        public string[] DislikedCategories { get; set; }
        public string Text { get; set; }
        public string PlaceId { get; set; }
    }
}