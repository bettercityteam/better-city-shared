﻿using Reinforced.Typings.Attributes;

namespace BetterCity.SharedKernel.Model.Request
{
    [TsClass]
    public class Places
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
        public double RadiusKm { get; set; }
    }
}