﻿using Reinforced.Typings.Attributes;

namespace BetterCity.SharedKernel.Model.Request
{
    [TsClass]
    public class ClientSignIn
    {
        public string Phone { get; set; }
        public string Code { get; set; }
    }
}