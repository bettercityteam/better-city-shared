﻿using Reinforced.Typings.Attributes;

namespace BetterCity.SharedKernel.Model.Request
{
    [TsClass]
    public class ReviewCategories
    {
        public string PlaceId { get; set; }
        public string[] Waypoints { get; set; }
    }
}