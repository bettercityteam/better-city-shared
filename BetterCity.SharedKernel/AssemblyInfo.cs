﻿using Reinforced.Typings.Attributes;

[assembly: TsGlobal(UseModules = true, RootNamespace = "BetterCityPublicApi", DiscardNamespacesWhenUsingModules = true, CamelCaseForProperties = true)]