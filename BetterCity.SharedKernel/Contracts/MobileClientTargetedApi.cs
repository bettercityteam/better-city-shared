﻿using System.Threading.Tasks;
using BetterCity.SharedKernel.Model.Request;
using BetterCity.SharedKernel.Model.Response;
using Reinforced.Typings.Attributes;

namespace BetterCity.SharedKernel.Contracts
{
    [TsInterface]
    public interface MobileClientTargetedApi
    {
        Task<PlaceGeometry[]> GetPlacesInArea(double latitude, double longitude, double radiusKm);
        Task<PlaceInfo> GetPlace(string placeId);
        Task<byte[]> GetImage(string id);
        Task<ReviewCategory[]> GetReviewCategories(string placeId, string[] waypointIds);
        Task<Review[]> GetOwnReviews(string placeId);
        Task<bool> CreateReview(NewReview review);
    }
}