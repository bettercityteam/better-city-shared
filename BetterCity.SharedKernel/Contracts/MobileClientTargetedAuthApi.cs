﻿using System.Threading.Tasks;
using Reinforced.Typings.Attributes;

namespace BetterCity.SharedKernel.Contracts
{
    [TsInterface]
    public interface MobileClientTargetedAuthApi
    {
        Task<bool> RequestCode(string phone);
        Task<bool> SignIn(string phone, string code);
    }
}