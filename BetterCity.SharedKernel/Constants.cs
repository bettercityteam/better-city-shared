﻿namespace BetterCity.SharedKernel
{
    public static class Constants
    {
        public static class Endpoints
        {
            public static class Api
            {
                public static class V1
                {
                    public const string BaseUrl = "",
                        GetPlacesInArea = "", 
                        GetReviewCategories = "",
                        CreateReview = "",
                        GetPlace = "",
                        GetImage = "",
                        GetOwnReviews = "";
                }
            }

            public static class Auth
            {
                public static class V1
                {
                    public const string
                        BaseUrl = "",
                        RequestCodeUrl = "",
                        SignIn = "";
                }
            }
        }
    }
}